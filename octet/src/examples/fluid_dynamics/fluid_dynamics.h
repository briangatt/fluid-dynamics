////////////////////////////////////////////////////////////////////////////////
//
//  Brian Gatt (ma301bg)
//
//  Real-Time Fluid Dynamics
//  Reference: http://www.autodeskresearch.com/publications/games
//  Reference: http://www.dgp.toronto.edu/people/stam/reality/Talks/FluidsTalk/FluidsTalk_files/v3_document.htm
//
////////////////////////////////////////////////////////////////////////////////

#ifndef FLUID_DYNAMICS_H_INCLUDED
#define FLUID_DYNAMICS_H_INCLUDED

#include "fluid_solver.h"
#include "fluid_utilities.h"

namespace octet {
namespace fluid {

  /** 
    *  Command Line Arguments
    */
  class CmdLineArgs {
  private:
    const char* _imageFile;

    util::Resolution2D _resolution;
    
    float _deltaTime;
    float _viscosity;
    //float _diffusion;
    float _force;

    bool _help;

    CmdLineArgs() :
      _imageFile(NULL),
      _resolution(64, 64),
      _deltaTime(0.1f),
      _viscosity(0.0f),
      //_diffusion(0.0f),
      _force(5.0f),
      _help(false) {
    };

    static util::Resolution2D parseResolution(const char* resolution) {
      util::Resolution2D result(64, 64);

      size_t dim = 0;
      const char* x = 0;
      for (const char* charPtr = resolution; *charPtr != '\0'; ++charPtr) {
        if (*charPtr == 'x') {
          x = charPtr;
          result.x = dim;
          dim = 0;
        } else if (*charPtr >= '0' && *charPtr <= '9') {
          dim = (dim * 10) + ((*charPtr) - '0');
        }
      }

      if (x != 0) {
        result.y = dim;
      }

      return result;
    };

  public:
    /**
      *  @return the --file|-f command line argument if specified or NULL
      **/
    const char* getImageFile() const {
      return _imageFile;
    };
    
    /**
      *  @return the --resolution|-r command line argument if specified or the default value
      **/
    util::Resolution2D getResolution() const {
      return _resolution;
    };

    /**
      *  @return the --dt|-t command line argument if specified or the default value
      **/
    float getDeltaTime() const {
      return _deltaTime;
    };
      
    /**
      *  @return the --viscosity|-v command line argument if specified or the default value
      **/
    float getViscosity() const {
      return _viscosity;
    };
      
    /**
      *  @return the --diffusion|-d command line argument if specified or the default value
      **/
    /*
    float getDiffusion() const {
      return _diffusion;
    };
    */
      
    /**
      *  @return the --force|-c command line argument if specified or the default value
      **/
    float getForce() const {
      return _force;
    };

    
    /**
      *  @return true if the --help|-h command line argument if specified
      **/
    bool isHelp() const {
      return _help;
    };

    static void printHelp(FILE* descriptor) {
      fprintf(descriptor, "  --file|-f [value]        Specify the fully qualified path to the texture to liquify.\n");
      fprintf(descriptor, "  --resolution|-r [value]  Specify the fluid discrete representation resolution in 'XxY' format.\n");
      fprintf(descriptor, "  --viscosity|-v [value]   Specify the fluid viscosity.\n");
      fprintf(descriptor, "  --force|-c [value]       Specify the force multiplier.\n");
      fprintf(descriptor, "  --dt|-t [value]          Specify the time delta between successive updates.\n");
      fprintf(descriptor, "  --help|-h                Displays this help screen.\n");
    };

    static CmdLineArgs parse(int argc, char* argv[]) {
      CmdLineArgs args;
        
      int i = 0;
      while (i < argc) {
        if (
            (::strcmp(argv[i], "--file") == 0 || ::strcmp(argv[i], "-f") == 0) &&
            argc >= i + 1
          ) {
          args._imageFile = argv[++i];
        } else if (
            (::strcmp(argv[i], "--resolution") == 0 || ::strcmp(argv[i], "-r") == 0) &&
            argc >= i + 1
          ) {
          args._resolution = parseResolution(argv[++i]);
        } else if (
            (::strcmp(argv[i], "--dt") == 0 || ::strcmp(argv[i], "-t") == 0) &&
            argc >= i + 1
          ) {
          args._deltaTime = (float) ::atof(argv[++i]);
        } else if (
            (::strcmp(argv[i], "--viscosity") == 0 || ::strcmp(argv[i], "-v") == 0) &&
            argc >= i + 1
          ) {
          args._viscosity = (float) ::atof(argv[++i]);
        /*
        } else if (
            (::strcmp(argv[i], "--diffusion") == 0 || ::strcmp(argv[i], "-d") == 0) &&
            argc >= i + 1
          ) {
          args._diffusion = (float) ::atof(argv[++i]);
        */
        } else if (
            (::strcmp(argv[i], "--force") == 0 || ::strcmp(argv[i], "-c") == 0) &&
            argc >= i + 1
          ) {
          args._force = (float) ::atof(argv[++i]);
        } else if (::strcmp(argv[i], "--help") == 0 || ::strcmp(argv[i], "-h") == 0) {
          args._help = true;
        }

        ++i;
      };

      return args;
    };
  };

  // --

  class fluid_dynamics_app : public app {
  private:
    // Copy of command line arguments
    CmdLineArgs _args;

    // Render velocities flag
    bool _renderVelocities;
    
    // Transformation matrices
    octet::mat4t _modelToWorld;
    octet::mat4t _cameraToWorld;

    // Screen image width, height
    float _width;
    float _height;

    // Shaders
    octet::color_shader _colour_shader;
    octet::texture_shader _texture_shader;

    // Image texture
    GLuint _textureID;

    // Fluid and Fluid simulator
    Fluid2D _fluid;
    FluidSimulator2D _simulator;

    Fluid2D::Density* _uDensity;
    Fluid2D::Density* _vDensity;

    // Old mouse X, Y positions
    int _mouseX;
    int _mouseY;
    
    // Position and UV attribute containers for density
    struct FluidGraphics {
      // Density Field
      octet::ref<octet::gl_resource> positionBuffer;
      octet::ref<octet::gl_resource> uvBuffer;
      octet::ref<octet::gl_resource> indexBuffer;
      
      // Velocity Field
      octet::ref<octet::gl_resource> velocityBuffer;

      // Anything inheriting from octet::resource should be allocated
      // on the heap due to 'delete this' statement in resource destructor.
      FluidGraphics() :
        positionBuffer(new gl_resource(GL_ARRAY_BUFFER)),
        uvBuffer(new gl_resource(GL_ARRAY_BUFFER)),
        indexBuffer(new gl_resource(GL_ELEMENT_ARRAY_BUFFER)),
        velocityBuffer(new gl_resource(GL_ARRAY_BUFFER)) {
      };
    } _fluidGraphics;

    // Input helper
    util::InputManager _input;
    
    void displayInfo() {
      fprintf(stdout, "\n");

      if (_args.getImageFile() == NULL) {
        fprintf(stderr, "Error: No image file specified as the --file|-f parameter.\n");
        CmdLineArgs::printHelp(stderr);
      } else {
        fprintf(stdout, "Using Image: %s\n", _args.getImageFile());
        fprintf(stdout, "Fluid Grid Resolution: [%d, %d]\n", _args.getResolution().x, _args.getResolution().y);
        fprintf(stdout, "Viscosity: %.5f\n", _args.getViscosity());
        //fprintf(stdout, "Diffusion: %.2f\n", _args.getDiffusion());
        fprintf(stdout, "Force: %.2f\n", _args.getForce());
        fprintf(stdout, "Delta Time: %.2f\n", _args.getDeltaTime());
      }

      fprintf(stdout, "\n");
    };

    void getMousePosition(float& x, float& y) {
      int mouseX = 0;
      int mouseY = 0;

      int viewportX = 0;
      int viewportY = 0;

      get_mouse_pos(mouseX, mouseY);
      get_viewport_size(viewportX, viewportY);
        
      x = (((float) mouseX) / ((float) viewportX));
      // Invert j - max y - y;
      y = (((float) (viewportY - mouseY)) / ((float) viewportY));
    };

    void handleInput() {
      // Move velocities
      if (is_key_down(key_lmb)) {
        int newMouseX = 0;
        int newMouseY = 0;

        int viewportX = 0;
        int viewportY = 0;

        get_mouse_pos(newMouseX, newMouseY);
        get_viewport_size(viewportX, viewportY);
        
        float newMouseXRatio = (((float) newMouseX) / ((float) viewportX));
        // Invert j - max y - y;
        float newMouseYRatio = (((float) (viewportY - newMouseY)) / ((float) viewportY));

        // Normalize position within grid range
        size_t i = (size_t) ::floor(newMouseXRatio * _args.getResolution().x);
        size_t j = (size_t) ::floor(newMouseYRatio * _args.getResolution().y);
        
        // The initial movement will have no effect until
        // _mouseX and _mouseY have been correctly initialised
        if (_mouseX < 0) {
          _mouseX = newMouseX;
        }
        if (_mouseY < 0) {
          _mouseY = newMouseY;
        }

        if (i > 0 && i < _args.getResolution().x && j > 0 && j < _args.getResolution().y) {
          
          util::Resolution2D paddedResolution = _fluid.getPaddedResolution();

          float& velocityX = util::getElement(
            paddedResolution,
            _simulator.getVelocityXSourceContainer(),
            i,
            j
          );

          // newMouseX - _mouseX since +ve values point to the East
          velocityX = _args.getForce() * (newMouseX - _mouseX);
        
          float& velocityY = util::getElement(
            paddedResolution,
            _simulator.getVelocityYSourceContainer(),
            i,
            j
          );
          
          // _MouseY - newMouseY since +ve values point to the North
          velocityY = _args.getForce() * (_mouseY - newMouseY);

        }

        // Keep a record of the current mouse position
        // to be able to calculate difference accordingly
        _mouseX = newMouseX;
        _mouseY = newMouseY;
      } else {
        // Reset mouse locations
        _mouseX = _mouseY = -1;
      }

      _input.poll();

      // Toggle velocity display
      if (_input.processKey('V')) {
        _renderVelocities = !_renderVelocities;
      // Reset fluid to original state
      } else if (_input.processKey(key_f5)) {
        reset();
      } 
    };
    
    void simulate() {
      for (size_t i = 0; i < _simulator.getVelocityXSourceContainer().size(); ++i) {
        _simulator.getVelocityXSourceContainer()[i] = _simulator.getVelocityYSourceContainer()[i] = 0.0f;
      }

      handleInput();

      _simulator.step(_args.getDeltaTime());
    };
    
    void updateUVCoordinates() {
      util::Resolution2D res = _fluid.getPaddedResolution();

      octet::gl_resource::rwlock uvLock(_fluidGraphics.uvBuffer);
      GLfloat* uv = (GLfloat*) uvLock.u8();
      size_t uvIndex = 0;

      for (size_t i = 1; i <= (_args.getResolution().x + 1); ++i) {
        for (size_t j = 1; j <= (_args.getResolution().y + 1); ++j) {
          
          // Translates from Structure of Array into Array of Structures in order
          // to organise them as expected by the shader program.
          uv[uvIndex++] = util::getElement(res, _uDensity->density.currentState, i, j);
          uv[uvIndex++] = util::getElement(res, _vDensity->density.currentState, i, j);

        }
      }
    };

    void initialiseDensityPositions() {
      octet::gl_resource::rwlock posLock(_fluidGraphics.positionBuffer);
      GLfloat* pos = (GLfloat*) posLock.u8();
      size_t index = 0;

      float xUnit = _width / (float) (_args.getResolution().x - 1);
      float yUnit = _height / (float) (_args.getResolution().y - 1);

      // Iterate over all point vertices
      for (size_t i = 0; i <= _args.getResolution().x; ++i) {
        float x = i * xUnit;
        for (size_t j = 0; j <= _args.getResolution().y; ++j) {

          pos[index++] = x;
          pos[index++] = j * yUnit;
          pos[index++] = 0;

        }
      }
    };

    void initialiseIndexBuffer() {
      octet::gl_resource::rwlock indexLock(_fluidGraphics.indexBuffer);
      GLuint* index = (GLuint*) indexLock.u8();
      size_t indexIndex = 0;
        
      // Iterate over all quad faces
      for (size_t i = 0; i < _args.getResolution().x; ++i) {
        size_t baseOffset = i * (_args.getResolution().y + 1);
        for (size_t j = 0; j < _args.getResolution().y; ++j) {
          size_t offset = baseOffset + j;

            // Triangle 1 in clockwise order
          index[indexIndex++] = offset;
          index[indexIndex++] = offset + 1;
          index[indexIndex++] = offset + _args.getResolution().y + 1;
          
          // Triangle 2 in clockwise order
          index[indexIndex++] = offset + 1;
          index[indexIndex++] = offset + _args.getResolution().y + 2;
          index[indexIndex++] = offset + _args.getResolution().y + 1;

        }
      }
    };

    void initialiseFluidBuffers() {
      size_t arrayBufferSize =  (_args.getResolution().x + 1) * (_args.getResolution().y + 1);
      size_t indexBufferSize =  _args.getResolution().x * _args.getResolution().y * 6;

      _fluidGraphics.positionBuffer->allocate(GL_ARRAY_BUFFER, arrayBufferSize * sizeof(GLfloat) * 3);
      _fluidGraphics.uvBuffer->allocate(GL_ARRAY_BUFFER, arrayBufferSize * sizeof(GLfloat) * 2);
      _fluidGraphics.indexBuffer->allocate(GL_ELEMENT_ARRAY_BUFFER, indexBufferSize * sizeof(GLuint));

      size_t velocityBufferSize = _args.getResolution().x * _args.getResolution().y * 2;

      _fluidGraphics.velocityBuffer->allocate(GL_ARRAY_BUFFER, velocityBufferSize * sizeof(GLfloat) * 3);

      initialiseDensityPositions();
      initialiseIndexBuffer();
    };

    void updateVelocityPositions() {
      util::Resolution2D res = _fluid.getPaddedResolution();
      
      octet::gl_resource::rwlock velocityLock(_fluidGraphics.velocityBuffer);
      GLfloat* velocity = (GLfloat*) velocityLock.u8();
      size_t index = 0;

      float xUnit = _width / (float) (_args.getResolution().x - 1);
      float yUnit = _height / (float) (_args.getResolution().y - 1);

      for (size_t i = 1; i <= _args.getResolution().x; ++i) {
        float x = ((float) i - 0.5f) * xUnit;
        for (size_t j = 1; j <= _args.getResolution().y; ++j) {
          float y = ((float) j - 0.5f) * yUnit;

          velocity[index++] = x;
          velocity[index++] = y;
          velocity[index++] = 0.0f;
          
          velocity[index++] = x + util::getElement(res, _fluid.getVelocityX().currentState, i, j);
          velocity[index++] = y + util::getElement(res, _fluid.getVelocityY().currentState, i, j);
          velocity[index++] = 0.0f;

        }
      }
    };

    void renderVelocities(const octet::mat4t& modelToProjection, const octet::vec4& colour) {
      updateVelocityPositions();

      _fluidGraphics.velocityBuffer->bind();
      glVertexAttribPointer(attribute_pos, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void *) 0);
      glEnableVertexAttribArray(attribute_pos);
    
      _colour_shader.render(modelToProjection, colour);

      glDrawArrays(GL_LINES, 0, _args.getResolution().x * _args.getResolution().y * 2);
    };

    void renderDensities(const octet::mat4t& modelToProjection) {
      // Bind grid vertices
      _fluidGraphics.positionBuffer->bind();
      glVertexAttribPointer(attribute_pos, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (void *) 0);
      glEnableVertexAttribArray(attribute_pos);
    
      // Update UV coordinates to reflect current fluid state
      updateUVCoordinates();

      // Bind UV coordinates
      _fluidGraphics.uvBuffer->bind();
      glVertexAttribPointer(attribute_uv, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), (void *) 0);
      glEnableVertexAttribArray(attribute_uv);
      
      // set up opengl to draw textured triangles using sampler 0 (GL_TEXTURE0)
      glActiveTexture(GL_TEXTURE0);
      glBindTexture(GL_TEXTURE_2D, _textureID);

      _texture_shader.render(modelToProjection, 0);
      
      // Bind index buffer
      _fluidGraphics.indexBuffer->bind();
      // Render
      glDrawElements(GL_TRIANGLES, _args.getResolution().x * _args.getResolution().y * 6, GL_UNSIGNED_INT, (void*) 0);
    };

    octet::mat4t project(const octet::mat4t& modelToWorld, const octet::mat4t& cameraToWorld) const {
      //return octet::mat4t::build_projection_matrix(_modelToWorld, _cameraToWorld);
      octet::mat4t worldToCamera;
      _cameraToWorld.invertQuick(worldToCamera);

      octet::vec4 zOrientation = cameraToWorld.z();
      
      // Orthographic projection on xy-plane
      octet::mat4t cameraToOrthoProjection(1.f);
      cameraToOrthoProjection.x() = octet::vec4(1 - util::square(zOrientation.x()), -(zOrientation.x() * zOrientation.y()), -(zOrientation.x() * zOrientation.z()), 0.f);
      cameraToOrthoProjection.y() = octet::vec4(-(zOrientation.x() * zOrientation.y()), 1 - util::square(zOrientation.y()), -(zOrientation.z() * zOrientation.y()), 0.f);
      cameraToOrthoProjection.z() = octet::vec4(-(zOrientation.x() * zOrientation.z()), -(zOrientation.y() * zOrientation.z()), 1 - util::square(zOrientation.z()), 0.f);

      return modelToWorld * worldToCamera * cameraToOrthoProjection;
    };

    void render(int x, int y, int w, int h) {
      // set a viewport - includes whole window area
      glViewport(x, y, w, h);

      // clear the background to black
      glClearColor(0, 0, 0, 1);
      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

      // disable Z buffer depth testing (Use painter's algorithm. Based on draw call order.)
      glDisable(GL_DEPTH_TEST);

      // allow alpha blend (transparency when alpha channel is 0)
      glEnable(GL_BLEND);
      glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

      octet::mat4t modelToProjection = project(_modelToWorld, _cameraToWorld);

      renderDensities(modelToProjection);

      if (_renderVelocities) {
        static octet::vec4 velocityColour(1.0f, 0.0f, 0.0f, 1.0f);
        renderVelocities(modelToProjection, velocityColour);
      }
    };

    void initialiseDensityFields() {
      
      _uDensity->density.clear();
      _vDensity->density.clear();

      util::Resolution2D paddedResolution = _fluid.getPaddedResolution();

      // -1 since for..loop is starting from 1
      float texelU = 1.0f / (float) (_args.getResolution().x - 1);
      float texelV = 1.0f / (float) (_args.getResolution().y - 1);

      // Starting from 1 to avoid padding introduced by Fluid2D data-structure
      // Including last value of x|y since loop performs -1 inside it
      for (size_t i = 1; i <= _args.getResolution().x; ++i) {
        for (size_t j = 1; j <= _args.getResolution().y; ++j) {
          // U coordinates
          util::getElement(paddedResolution, _uDensity->density.currentState, i, j) = texelU * (i - 1);
          util::getElement(paddedResolution, _uDensity->density.previousState, i, j) = texelU * (i - 1);

          // V coordinates
          util::getElement(paddedResolution, _vDensity->density.currentState, i, j) = texelV * (j - 1);
          util::getElement(paddedResolution, _vDensity->density.previousState, i, j) = texelV * (j - 1);
        }
      }

    };

    void printDensityState() {
#ifdef _DEBUG
 
      printf("%d\n", _uDensity->density.currentState.size());
      for (size_t i = 0; i < _uDensity->density.currentState.size(); ++i) {
        printf("%.2f, ", _uDensity->density.currentState[i]);
      }
      printf("\n");
      
      printf("%d\n", _vDensity->density.currentState.size());
      for (size_t i = 0; i < _vDensity->density.currentState.size(); ++i) {
        printf("%.2f, ", _vDensity->density.currentState[i]);
      }
      printf("\n");

#endif
    };

  public:
    fluid_dynamics_app(int argc, char **argv, const CmdLineArgs& args) :
      app(argc, argv),
      _args(args),
      _renderVelocities(false),
      _modelToWorld(1.0f),
      _cameraToWorld(1.0f),
      _fluid(util::Resolution2D()),
      _simulator(&_fluid),
      _uDensity(NULL),
      _vDensity(NULL),
      _mouseX(-1),
      _mouseY(-1),
      _input() {
        
    }

    void reset() {
      _fluid.reset(_args.getResolution(), _args.getViscosity());
      _fluid.clear();

      // Reset simulator upon resetting fluid data structure to
      // ensure that simulator has seen the latest changes
      _simulator.reset();

      // u, v density representations should not diffuse by themselves.
      _uDensity = &_fluid.addDensity(0.0f); //_args.getDiffusion());
      _vDensity = &_fluid.addDensity(0.0f); //_args.getDiffusion());
      
      initialiseDensityFields();
      printDensityState();

      octet::ref<octet::image> texture(new image(_args.getImageFile()));
      _textureID = texture->get_gl_texture();

      initialiseFluidBuffers();
    };

    /// this is called once OpenGL is initialized
    void app_init() {
      if (_args.isHelp()) {
        printf("Liquid Textures\n\n");
        CmdLineArgs::printHelp(stdout);
        printf("\n");
      }

      // Display initial information
      displayInfo();

      // Attach input helper
      _input.attach(this);
      
      //_modelToWorld.loadIdentity();
      //_cameraToWorld.loadIdentity();
      _cameraToWorld.translate(1.f, 1.f, 1.f);

      _width = 2.0f;
      _height = 2.0f;

      // Initialise shaders
      _colour_shader.init();
      _texture_shader.init();

      // Reset the fluid simulator and other necessary data structures
      reset();
    }

    /// this is called to draw the world
    void draw_world(int x, int y, int w, int h) {
      simulate();
      render(x, y, w, h);
    }
  };

} // namespace fluid
} // namespace octet

#endif // FLUID_DYNAMICS_H_INCLUDED