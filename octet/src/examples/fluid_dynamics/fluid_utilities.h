////////////////////////////////////////////////////////////////////////////////
//
//  Brian Gatt (ma301bg)
//
//  Real-Time Fluid Dynamics
//  Reference: http://www.autodeskresearch.com/publications/games
//  Reference: http://www.dgp.toronto.edu/people/stam/reality/Talks/FluidsTalk/FluidsTalk_files/v3_document.htm
//
////////////////////////////////////////////////////////////////////////////////

#ifndef FLUID_UTILITIES_H_INCLUDED
#define FLUID_UTILITIES_H_INCLUDED

namespace octet {
namespace fluid {
namespace util {

  template <typename T>
  inline T square(const T& x) {
    return x * x;
  };

  // --

  template <typename T>
  T lerp(const T& begin, const T& end, float t) {
    return ((1.f - t) * begin) + (t * end);
  };
  
  // --

  // ^ p01 -- p11
  // |  |      |
  // |  |      |
  // V p00 -- p10
  //    U ---->
  template <typename T>
  T bilerp(const T& p00, const T& p10, const T& p11, const T& p01, float u, float v) {
    return lerp(lerp(p00, p10, u), lerp(p01, p11, u), v);
  };
  
  // --

  // VERTICES:
  //
  //    .h------g
  //  .' |    .'|
  // d---+--c'  |
  // |   |  |   |
  // |  ,e--+---f
  // |.'    | .'
  // a------b'
  // 
  // AXIS:
  //
  // v
  // |  
  // |  ,w
  // |.'
  // +------u
  template <typename T>
  T trilerp(const T& a, const T& b, const T& c, const T& d, const T& e, const T& f, const T& g, const T& h, float u, float v, float w) {
    return lerp(bilerp(a, b, c, d, u, v), bilerp(e, f, g, h, u, v), w);
  };
  
  // --

  template <typename T>
  T clamp(const T& value, const T& min, const T& max) {
    return std::max(min, std::min(max, value));
  };

  // --

  struct Colour {
    // Reference: app_utils.h
    static vec4 parse(const char* hex) {
      unsigned val = 0;
      unsigned ndigits = 0;
      for (int i = 0; hex[i]; ++i) {
        char c = toupper(hex[i]);
        val = val * 16 + ( ( c <= '9' ? c - '0' : c - 'A' + 10 ) & 0x0f );
        ndigits++;
      }

      if (ndigits == 8) {
        val >>= 8;
      }
      
      return vec4(
        ((val >> 16) & 0xFF) / 255.f,
        ((val >> 8) & 0xFF) / 255.f,
        ((val >> 0) & 0xFF) / 255.f,
        1.f
      );
    };
  };

  // --

  /**
   * A static equivalent to an octet::dynarray.
   */
  template <typename T>
  class StaticArray {
  public:
    typedef size_t size_t;
    typedef T item_type_t;

  private:
    size_t _size;
    item_type_t* _data;

    void _copy(const StaticArray& rhs) {
      ::memcpy(_data, rhs._data, rhs.size() * sizeof(item_type_t));
    };

  public:
    StaticArray(const size_t size = 0) :
      _size(size),
      _data(new item_type_t[_size]) {
    };

    ~StaticArray() {
      delete[] _data;
    };

    StaticArray(const StaticArray& rhs) :
      _size(rhs.size()),
      _data(new item_type_t[_size]) {
        _copy(rhs);
    };

    StaticArray& operator=(const StaticArray& rhs) {
      reset(rhs.size());
      _copy(rhs);

      return *this;
    };

    size_t size() const {
      return _size;
    };

    void reset(size_t size) {
      delete[] _data;

      _size = size;
      _data = new item_type_t[_size];
    };

    // -- NOTE --
    // 
    // The following operator[] implementations turn out to be
    // hotspots in our code and tend to slow down the application
    // when using these abstractions.
    //
    // ESPECIALLY WHEN COMPILING IN DEBUG MODE.
    // Release mode is !much! faster.

    item_type_t& operator[](size_t index) {
      return _data[index];
    };
    
    const item_type_t& operator[](size_t index) const {
      return _data[index];
    };

    item_type_t* data() {
      return _data;
    };

    void swap(StaticArray& rhs) {
      std::swap(_size, rhs._size);
      std::swap(_data, rhs._data);
    };
  };

  // --

  struct Resolution2D {
    size_t x;
    size_t y;

    Resolution2D(size_t x = 0, size_t y = 0) :
      x(x),
      y(y) {
    }
  };

  // --
  
  // -- NOTE --
  // 
  // The following getElement implementations turn out to be
  // hotspots in our code and tend to slow down the application
  // when using these abstractions.
  //
  // ESPECIALLY WHEN COMPILING IN DEBUG MODE.
  // Release mode is !much! faster.

  template <typename T>
  static inline const T& getElement(const util::Resolution2D& resolution, const util::StaticArray<T>& container, size_t x, size_t y) {
    return container[x + (resolution.x * y)];
  }
  
  template <typename T>
  static inline T& getElement(const util::Resolution2D& resolution, util::StaticArray<T>& container, size_t x, size_t y) {
    return container[x + (resolution.x * y)];
  }

  // --

  /*
  template <typename T>
  static void zero(util::StaticArray<T>& container) {
    for (size_t i = 0; i < container.size(); ++i) {
      container[i] = T(0);
    };
  };
  */
  
  template <typename T>
  static void zero(util::StaticArray<T>& container) {
    ::memset(container.data(), 0, container.size() * sizeof(T));
  };

  // --

  // Utility class which manages input.
  // Used to delay key-press.
  class InputManager {
  private:
    app* _app;

    // The key delay amount per poll request
    unsigned int _keyDelay;

    // Delay accumulator for processing key presses
    unsigned int _delay;

    // Non-copyable entity
    InputManager(const InputManager&);
    InputManager& operator=(const InputManager&);

  public:
    typedef unsigned int keycode;

    InputManager(app* app = NULL, unsigned int keyDelay = 5) :
      _app(app),
      _keyDelay(keyDelay),
      _delay(0) {
    };

    ~InputManager() {
    };

    void attach(app* app) {
      _app = app;
      _delay = 0;
    };

    void setKeyDelay(unsigned int keyDelay) {
      _keyDelay = keyDelay;
      _delay = 0;
    };

    bool isKeyDown(keycode key) const {
      return _delay == 0 && _app != NULL && _app->is_key_down(key);
    };

    bool processKey(keycode key) {
      if (isKeyDown(key)) {
        _delay = _keyDelay;
        return true;
      };

      return false;
    };

    // Check for key-presses
    void poll() {
      _delay = (_delay == 0 ? 0 : _delay - 1);
    };
  };

  // --
  
} // namespace util
} // namespace fluid
} // namespace octet

#endif // FLUID_UTILITIES_H_INCLUDED