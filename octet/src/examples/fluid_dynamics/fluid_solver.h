////////////////////////////////////////////////////////////////////////////////
//
//  Brian Gatt (ma301bg)
//
//  Real-Time Fluid Dynamics
//  Reference: http://www.autodeskresearch.com/publications/games
//  Reference: http://www.dgp.toronto.edu/people/stam/reality/Talks/FluidsTalk/FluidsTalk_files/v3_document.htm
//
////////////////////////////////////////////////////////////////////////////////

#ifndef FLUID_SOLVER_H_INCLUDED
#define FLUID_SOLVER_H_INCLUDED

#include "fluid_utilities.h"

namespace octet {
namespace fluid {

  class Fluid2D {
  public:
    struct Field {
      util::StaticArray<float> currentState;
      util::StaticArray<float> previousState;

      Field(size_t size) :
        currentState(size),
        previousState(size) {
      };

      void clear() {
        for (size_t i = 0; i < currentState.size(); ++i) {
          currentState[i] = previousState[i] = 0.0f;
        }
      };

      void swap() {
        currentState.swap(previousState);
      };
    };

    class Density {
    public:
      float diffusion;
      Field density;
      
      // Required due to dynarray specification
      Density() :
        diffusion(0.0f),
        density(0) {
      };

    private:
      friend class Fluid2D;

      Density(float diffusion, const util::Resolution2D& resolution) :
        diffusion(diffusion),
        density(Fluid2D::getSize(resolution)) {
      }
    };

  private:
    // Stores the x and y component resolutions
    util::Resolution2D _resolution;
    
    // The fluid viscosity
    float _viscosity;

    // Stores the x component of the velocity vector for each discrete cell
    Field _velocityX;

    // Stores the y component of the velocity vector for each discrete cell
    Field _velocityY;

    // The densities flowing throughout this fluid container
    octet::dynarray<Density> _densities;

    static size_t addPadding(size_t original) {
      return original + 2;
    };

    static size_t getSize(const util::Resolution2D& resolution) {
      return addPadding(resolution.x) * addPadding(resolution.y);
    };

  public:
    explicit Fluid2D(const util::Resolution2D& resolution, float viscosity = 0.0f) :
      _resolution(resolution),
      _viscosity(viscosity),
      _velocityX(getSize(resolution)),
      _velocityY(getSize(resolution)),
      _densities() {
    };

    ~Fluid2D() {
    };
    
    void reset(const util::Resolution2D& resolution) {
      reset(resolution, _viscosity);
    };

    void reset(const util::Resolution2D& resolution, float viscosity) {
      _resolution = resolution;
      _velocityX = Field(getSize(resolution));
      _velocityY = Field(getSize(resolution));
      
      _viscosity = viscosity;

      _densities.reset();
    };
    
    void clear() {
      _velocityX.clear();
      _velocityY.clear();

      for (size_t i = 0; i < _densities.size(); ++i) {
        _densities[i].density.clear();
      }
    };

    util::Resolution2D getPaddedResolution() const {
      return util::Resolution2D(addPadding(_resolution.x), addPadding(_resolution.y));
    };

    const util::Resolution2D& getResolution() const {
      return _resolution;
    };
    
    void setViscosity(float viscosity) {
      _viscosity = viscosity;
    };

    float getViscosity() const {
      return _viscosity;
    };

    const Field& getVelocityX() const {
      return _velocityX;
    };

    Field& getVelocityX() {
      return _velocityX;
    };
    
    const Field& getVelocityY() const {
      return _velocityY;
    };

    Field& getVelocityY() {
      return _velocityY;
    };

    const octet::dynarray<Density>& getDensities() const {
      return _densities;
    };

    octet::dynarray<Density>& getDensities() {
      return _densities;
    };

    Density& addDensity(float diffusion) {
      getDensities().push_back(Density(diffusion, getResolution()));
      return getDensities().back();
    };
  };

  class FluidSimulator2D {
  private:
    enum Axis { UNDEFINED = -1, X = 0, Y = 1 };

    Fluid2D* _fluid;
    
    util::Resolution2D _paddedResolution;

    static size_t getRelaxationIterationCount() {
      return 20;
    };
    
    // Returns the user-specified resolution along the x axis
    inline size_t sizeX() const {
      return _fluid->getResolution().x;
    }
    
    // Returns the user-specified resolution along the y axis
    inline size_t sizeY() const {
      return _fluid->getResolution().y;
    };
    
    // -- NOTE --
    // 
    // The following getElement implementation turns out to be a
    // hotspot in our code and tends to slow down the application.
    //
    // ESPECIALLY WHEN COMPILING IN DEBUG MODE.
    // Release mode is !much! faster.

    template <typename T>
    inline T& getElement(util::StaticArray<T>& arr, size_t x, size_t y) const {
      return util::getElement(_paddedResolution, arr, x, y);
    };

    void addSources(util::StaticArray<float>& x, util::StaticArray<float>& s, float deltaTime) {
      for (size_t i = 0; i < x.size(); ++i) {
        x[i] += deltaTime * s[i];
      }
    };

    // Fixed boundary conditions i.e. fluid is contained within the specified boundary
    void setBoundary(Axis axis, util::StaticArray<float>& arr) {
      // Set Y axis elements along X axis boundaries
      for (size_t i = 1; i <= sizeY(); ++i) {
        getElement(arr, 0, i) = (axis == X ? -getElement(arr, 1, i) : getElement(arr, 1, i));
        getElement(arr, sizeX() + 1, i) = (axis == X ? -getElement(arr, sizeX(), i) : getElement(arr, sizeX(), i));
      }
      
      // Set X axis elements along Y axis boundaries
      for (size_t i = 1; i <= sizeX(); ++i) {
        getElement(arr, i, 0) = (axis == Y ? -getElement(arr, i, 1) : getElement(arr, i, 1));
        getElement(arr, i, sizeY() + 1) = (axis == Y ? -getElement(arr, i, sizeY()) : getElement(arr, i, sizeY()));
      }
      
      // Average corner elements with two direct Von Neumann neighbours
      getElement(arr, 0, 0) = 0.5f * (getElement(arr, 1, 0) + getElement(arr, 0, 1));
      getElement(arr, 0, sizeY() + 1) = 0.5f * (getElement(arr, 1, sizeY() + 1) + getElement(arr, 0, sizeY()));
      getElement(arr, sizeX() + 1, 0) = 0.5f * (getElement(arr, sizeX(), 0) + getElement(arr, sizeX() + 1, 1));
      getElement(arr, sizeX() + 1, sizeY() + 1) = 0.5f * (getElement(arr, sizeX(), sizeY() + 1) + getElement(arr, sizeX() + 1, sizeY()));
    };

    void linearSolve(Axis axis, util::StaticArray<float>& x, util::StaticArray<float>& x0, float a, float c) {
      // Gauss-Siedl relaxation algorithm
      // Iterates until unknown values converge to a stable result
	    for (size_t k = 0; k < getRelaxationIterationCount(); ++k) {
        for (size_t i = 1; i <= sizeX(); ++i) {
          for (size_t j = 1; j <= sizeY(); ++j) {

            getElement(x, i, j) = (getElement(x0, i, j) + a * (getElement(x, i - 1, j) + getElement(x, i + 1, j) + getElement(x, i, j - 1) + getElement(x, i, j + 1))) / c;

          }
        }
        
        setBoundary(axis, x);
	    }
    };

    void diffuse(
      Axis axis,
      util::StaticArray<float>& x,
      util::StaticArray<float>& x0,
      float diff,
      float deltaTime
    ) {
      float diffusionRate = deltaTime * diff * sizeX() * sizeY();
      linearSolve(axis, x, x0, diffusionRate, 1 + (4 * diffusionRate));
    };

    void advect(
      Axis axis,
      util::StaticArray<float>& d,
      util::StaticArray<float>& d0,
      util::StaticArray<float>& u,
      util::StaticArray<float>& v,
      float deltaTime
    ) {
      float dt0x = deltaTime * sizeX();
      float dt0y = deltaTime * sizeY();
      
      for (size_t i = 1; i <= sizeX(); ++i) {
        for (size_t j = 1; j <= sizeY(); ++j) {
          // Find the particle locations by tracing back in time
          float x = i - dt0x * getElement(u, i, j); 
          float y = j - dt0y * getElement(v, i, j);

          x = util::clamp(x, 0.5f, sizeX() + 0.5f);
          y = util::clamp(y, 0.5f, sizeY() + 0.5f);

          unsigned int segmentX = (unsigned int) ::floor(x);
          float localX = x - segmentX;

          unsigned int segmentY = (unsigned int) ::floor(y);
          float localY = y - segmentY;

          // Interpolate
          getElement(d, i, j) = util::bilerp(
            getElement(d0, segmentX, segmentY), 
            getElement(d0, segmentX + 1, segmentY), 
            getElement(d0, segmentX + 1, segmentY + 1), 
            getElement(d0, segmentX, segmentY + 1),
            localX,
            localY
          );
        }
      }

      setBoundary(axis, d);
    };

    void project(util::StaticArray<float>& u, util::StaticArray<float>& v, util::StaticArray<float>& p, util::StaticArray<float>& div) {
      for (size_t i = 1; i <= sizeX(); ++i) {
        for (size_t j = 1; j <= sizeY(); ++j) {
          // Calculate gradient field
          // NOTE These outside brackets 'v' are !important! since we take 0.5 of the resultant addition not just the first value.  
          getElement(div, i, j) = -0.5f * ( ((getElement(u, i + 1, j) - getElement(u, i - 1, j)) / sizeX()) + ((getElement(v, i, j + 1) - getElement(v, i, j - 1)) / sizeY()) );
          getElement(p, i, j) = 0.0f;
        }
      }

      setBoundary(UNDEFINED, div);
      setBoundary(UNDEFINED, p);

      // Solve the gradient field in a stable manner 
      linearSolve(UNDEFINED, p, div, 1, 4);
      
      // Subtract gradient field from current velocity field
      for (size_t i = 1; i <= sizeX(); ++i) {
        for (size_t j = 1; j <= sizeY(); ++j) {
          getElement(u, i, j) -= 0.5f * sizeX() * (getElement(p, i + 1, j) - getElement(p, i - 1, j));
          getElement(v, i, j) -= 0.5f * sizeY() * (getElement(p, i, j + 1) - getElement(p, i, j - 1));
        }
      }
      
      setBoundary(X, u);
      setBoundary(Y, v);
    };

    void velocityStep(float deltaTime) {
      // Velocity step applies each operation along each dimensional axis

      addSources(_fluid->getVelocityX().currentState, _fluid->getVelocityX().previousState, deltaTime);
      addSources(_fluid->getVelocityY().currentState, _fluid->getVelocityY().previousState, deltaTime);

      // Swap is used to temporarily switch operation buffers and affect where the results will be stored.
      // This is mainly used to conserve memory and to avoid creating new buffers if possible.
      //
      // Therefore, current state will now actually be the previous state. For efficiency purposes, semantics
      // are being ignored temporarily.
      _fluid->getVelocityX().swap();
      diffuse(X, _fluid->getVelocityX().currentState, _fluid->getVelocityX().previousState, _fluid->getViscosity(), deltaTime);

      _fluid->getVelocityY().swap();
      diffuse(Y, _fluid->getVelocityY().currentState, _fluid->getVelocityY().previousState, _fluid->getViscosity(), deltaTime);

      // Conserve mass
      project(_fluid->getVelocityX().currentState, _fluid->getVelocityY().currentState, _fluid->getVelocityX().previousState, _fluid->getVelocityY().previousState);
     
      // NOTE This swap operation will shift back arrays to their original semantics.
      //      The 'currentState' value will now actually refer to the current state.
      _fluid->getVelocityX().swap();
      _fluid->getVelocityY().swap();
      // Allow velocity to move under the effect of itself
      advect(X, _fluid->getVelocityX().currentState, _fluid->getVelocityX().previousState, _fluid->getVelocityX().previousState, _fluid->getVelocityY().previousState, deltaTime);
      advect(Y, _fluid->getVelocityY().currentState, _fluid->getVelocityY().previousState, _fluid->getVelocityX().previousState, _fluid->getVelocityY().previousState, deltaTime);
      
      // Again, conserve mass
      project(_fluid->getVelocityX().currentState, _fluid->getVelocityY().currentState, _fluid->getVelocityX().previousState, _fluid->getVelocityY().previousState);
    };

    void densityStep(Fluid2D::Density& density, float deltaTime) {
      // NOTE For our application, we do not make use of this part of the equation. Densities cannot be added.
      //addSources(density.density.currentState, getDensitySourceContainer(density), deltaTime);
      
      // Diffuse the density i.e. naturally spread the (mass of the) density along the area.
      density.density.swap();
      diffuse(
        UNDEFINED,
        density.density.currentState,
        density.density.previousState,
        density.diffusion,
        deltaTime
      );

      // Allow density to move under the effect of the velocity field
      density.density.swap();
      advect(UNDEFINED, density.density.currentState, density.density.previousState, _fluid->getVelocityX().currentState, _fluid->getVelocityY().currentState, deltaTime);
    };

  public:
    FluidSimulator2D(Fluid2D* fluid) :
      _fluid(fluid),
      _paddedResolution(_fluid->getPaddedResolution()) {
    };

    void reset() {
      reset(_fluid);
    };

    void reset(Fluid2D* fluid) {
      _fluid = fluid;
      _paddedResolution = _fluid->getPaddedResolution();
    };

    ~FluidSimulator2D() {
    };
    
    util::StaticArray<float>& getVelocityXSourceContainer() {
      return _fluid->getVelocityX().previousState;
    };

    util::StaticArray<float>& getVelocityYSourceContainer() {
      return _fluid->getVelocityY().previousState;
    };

    util::StaticArray<float>& getDensitySourceContainer(Fluid2D::Density& density) {
      return density.density.previousState;
    };

    void step(float deltaTime) {
      velocityStep(deltaTime);

      for (size_t i = 0; i < _fluid->getDensities().size(); ++i) {
        densityStep(_fluid->getDensities()[i], deltaTime);
      }
    };
  };

} // namespace fluid
} // namespace octet

#endif // FLUID_SOLVER_H_INCLUDED