////////////////////////////////////////////////////////////////////////////////
//
//  Brian Gatt (ma301bg)
//
//  Real-Time Fluid Dynamics
//  Reference: http://www.autodeskresearch.com/publications/games
//  Reference: http://www.dgp.toronto.edu/people/stam/reality/Talks/FluidsTalk/FluidsTalk_files/v3_document.htm
//
////////////////////////////////////////////////////////////////////////////////

#include "../../octet.h"

#include "fluid_dynamics.h"

int main(int argc, char **argv) {
  octet::fluid::CmdLineArgs args = octet::fluid::CmdLineArgs::parse(argc, argv);

  // Disabling prefix since user is expected to enter the
  // fully qualified path of the image he/she wishes to use.

  // path from bin\Debug to octet directory
  octet::app_utils::prefix("");

  // set up the platform.
  octet::app::init_all(argc, argv);

  // our application.
  octet::fluid::fluid_dynamics_app app(argc, argv, args);
  app.init();

  // open windows
  octet::app::run_all_apps();
}